def getMinSpeed(distance):
    if distance >= 0 and distance <= 600:
        return 15
    elif distance <= 1000:
        return 11.428
    elif distance <= 1300:
        return 13.333
    else: return None

def getMaxSpeed(distance):
    if distance >= 0 and distance <= 200:
        return 34
    elif distance <= 400:
        return 32
    elif distance <= 600:
        return 30
    elif distance <= 1000:
        return 28
    elif distance <= 1300:
        return 26
    else: return None

def getOpeningTime(brevetDistance, controleDistance):
    timeString = ""
    time = controleDistance / getMaxSpeed(brevetDistance)
    timeString += str(time).split('.')[0] + "H"
    dec = '.' + str(time).split('.')[1]
    min = str(round(float(dec) * 60))
    if len(min) == 1:
        timeString += "0" + min 
    else: timeString += min
    return timeString

def getClosingTime(brevetDistance, controleDistance):
    
    if brevetDistance == 0:
        return "1H00"

    timeString = ""
    time = controleDistance / getMinSpeed(brevetDistance)
    timeString += str(time).split('.')[0] + "H"
    dec = '.' + str(time).split('.')[1]
    min = str(round(float(dec) * 60))
    if len(min) == 1:
        timeString += "0" + min 
    else: timeString += min
    return timeString

def addTimes(distances):
    hrs = 0
    min = 0
    for s in distances:
        hrs += int(s.split('H')[0])
        min += int(s.split('H')[1])
    
    while min >= 60:
        hrs += 1
        min -= 60
    
    timeStr = str(hrs) + "H"
    if len(str(min)) == 1:
        timeStr += "0"
    timeStr += str(min)
    return timeStr


