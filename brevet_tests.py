import unittest
import brevetCalculations

class TestCases(unittest.TestCase):
    def test_min_distance(self):
        self.assertEqual(brevetCalculations.getMinSpeed(100), 15)
        self.assertEqual(brevetCalculations.getMinSpeed(700), 11.428)
        self.assertEqual(brevetCalculations.getMinSpeed(1100), 13.333)
        pass

    def test_max_distance(self):
        self.assertEqual(brevetCalculations.getMaxSpeed(100), 34)
        self.assertEqual(brevetCalculations.getMaxSpeed(300), 32)
        self.assertEqual(brevetCalculations.getMaxSpeed(700), 28)
        pass

    def test_get_open_time(self):
        self.assertEqual(brevetCalculations.getOpeningTime(60), "1H46")
        self.assertEqual(brevetCalculations.getOpeningTime(175), "5H09")
        self.assertEqual(brevetCalculations.getOpeningTime(200), "5H53")

    def test_get_close_time(self):
        self.assertEqual(brevetCalculations.getClosingTime(60), "4H00")
        self.assertEqual(brevetCalculations.getClosingTime(120), "8H00")
        self.assertEqual(brevetCalculations.getClosingTime(175), "11H40")

    def test_add_times(self):
        self.assertEqual(brevetCalculations.addTimes(["1H00", "1H00", "1H00"]), "3H00")
        self.assertEqual(brevetCalculations.addTimes(["2H10", "1H05", "3H30"]), "6H45")
        self.assertEqual(brevetCalculations.addTimes(["3H30", "3H10", "3H30"]), "10H10")

if __name__ == "__main__":
    unittest.main()