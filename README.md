# proj3-ajax
Reimplementation of the RUSA ACP controle time calculator with flask and ajax

#Author
Emmalie Dion

Bitbucket link:https://EmDion14@bitbucket.org/EmDion14/proj4-ajax.git
ix path: home/users/emmalie/proj4-ajax

## Program

The program is a reimplementation of the RUSA ACP controle time calculator. The user enters a brevet distance (200, 300, 400, 600, or 1000km), starting date, and starting time. The program then uses the calculation rules on the RUSA site (http://www.rusa.org/octime_alg.html) in order to calculate the start and end times for each controle that the user enters into the table. The closing times are calculated using the minimum speed and the opening times are calculated using the maximum speed. In this case 400 uses the values in the third row of the table, 600 uses the values in the fourth row, and 1000 uses the values in the fifth row of the table.


##Issues
I got sick and was not able to work on the project for a few days. I received an extension but unfortunately I was still not able to complete as much of the project as I wanted to. 

##What I was able to complete
My program has a different table style than the original. It allows for 10 controls. There are text boxes for user input of Brevet distance, starting date, and starting time, but unfortunately none of them are connected to the program. Because of this I only have basic calculations that correspond to a 200km brevet. The AJAX fills in the table as the user types in distances for controls. Unfortunately the times display is not very good. I could not get the formatting to work as it should. Also the first controle at zero does not set to an hour after start time. When it shows the times some of them show negative seconds. When comparing to the actual website it matches with the times but it is written wrong. It also does not stop at the brevet distance. It will continue to calculate no matter what you type for controle. 
